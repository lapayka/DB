select work from Users;

select work->>'salary' from users;

select c.work->'experience' is not null as has_experience
from users c;

update users
set work = '{"salary":10000, "experience":1}'
where users.credHistory = 1;

alter table users add salary int;
alter table users add experience int;

update users
set salary = cast(work->>'salary' as int);

update users
set experience = cast(work->>'experience' as int);

select * from users;

-- процедура по банку в json карту и пользователля

CREATE OR REPLACE PROCEDURE p2(abname varchar)
AS $$
    query = "select c.id, u.passport from (select * from banks where banks.name = '{0}') b inner join Cards c on c.bname = b.name inner join users u on u.passport = c.passport".format(abname)
    arr = plpy.execute(query)

    import json
    
    for i in arr:
        j = json.dumps({"id": arr["id"], "pass": arr["passport"]})
        jarr.append(j)
        plpy.notice(str(j))
        

$$ LANGUAGE plpython3u;