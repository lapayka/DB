create or replace function getWork()
returns jsonb
as $$
    from random import randint
    import json
    
    return json.dumps({"experience": randint(0, 20), "salary": randint(20000, 300000)})
$$ LANGUAGE plpython3u;

alter table users add work jsonb;

update Users
set work = getWork();

select * from users;