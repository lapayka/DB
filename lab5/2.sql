create table if not exists UsersJSON(
	name varchar(30),
	birthDate date,
	passport bigint,
    creditHistory int
);

alter table UsersJSON add primary key(passport);
alter table UsersJSON add constraint birthDate check(birthDate is not null);
alter table UsersJSON add constraint passport check(passport >= 4500000000 and passport <= 4599999999);
alter table UsersJSON add constraint credHistory check (credHistory >= 0 and credHistory <= 10);

CREATE TABLE tmp (
    data jsonb
);
\COPY tmp (data) FROM '/home/denis/study/DB/DV/data/users.json';

INSERT INTO UsersJSON(name, birthDate, passport, creditHistory)
select
  data->>'name',
  cast(data->>'birthdate' as date),
  cast(data->>'passport' as bigint),
  cast(data->>'credhistory' as int)
FROM tmp;

select * from UsersJSON;