drop table Banks, Users, Transactions, Cards;

create table if not exists Banks(
	name VARCHAR(50),
	address VARCHAR(70),
	lim int,
	amount bigint,
	parentName VARCHAR(50)
);

create table if not exists Users(
	name varchar(30),
	birthDate date,
	passport bigint,
    credHistory int
);

create table if not exists Transactions(
	id int, 
	passport bigint,
	bName varchar(50),
	cDate date,
    ttype varchar(20),
    tsum integer
);

create table if not exists Cards(
	id int, 
	passport bigint,
	bName varchar(50),
	cardType varchar(20),
	pSystem VARCHAR(15),
	amount int,
	cDate date
);
