alter table Banks add primary key(name);
alter table Banks add constraint name check(name is not null);
alter table Banks add constraint address check(address is not null);
alter table Banks add constraint lim check (lim >= 0 and lim <= 100000);
alter table Banks add constraint amount check (amount >= 0 and amount <= 1000000000);

alter table Users add primary key(passport);
alter table Users add constraint birthDate check(birthDate is not null);
alter table Users add constraint passport check(passport >= 4500000000 and passport <= 4599999999);
alter table Users add constraint credHistory check (credHistory >= 0 and credHistory <= 10);

alter table Transactions add primary key(id);
alter table Transactions add foreign key (passport) references Users(passport);
alter table Transactions add foreign key (bName) references Banks(name);
alter table Transactions add constraint cDate check(cDate is not null);
alter table Transactions add constraint ttype check(ttype is not null);

alter table Cards add primary key(id);
alter table Cards add foreign key (passport) references Users(passport);
alter table Cards add foreign key (bName) references Banks(name);
alter table Cards add constraint cDate check(cDate is not null);
alter table Cards add constraint cardType check(cardType is not null);
alter table Cards add constraint pSystem check(pSystem is not null);
