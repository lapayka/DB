create type person as
(
    name varchar,
    passport varchar
);
create or replace function getPersons()
returns setof person
as
$$
    arr = []
    result = plpy.execute("select name, passport from users")
    for i in result:
        arr.append([ i['name'], i['passport'] ])    
    return tuple(arr)
$$ language plpython3u;

select * from getPersons()