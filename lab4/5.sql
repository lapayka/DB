drop trigger trigger_insert on cardsview;
create or replace function  delete_invalid_cards() returns trigger
as $$
    query = "select birthdate from users where users.passport = {0};".format(TD["new"]["passport"])
    res = plpy.execute(query)
    if (TD["new"]["cdate"] > res[0]["birthdate"]):
        query = f"insert into cards values ({0}, {1}, {2}, {3}, {4}, {5}, {6});".format(TD["new"]["id"], TD["new"]["passport"], TD["new"]["bname"], TD["new"]["cardtype"], TD["new"]["psystem"], TD["new"]["amount"], TD["new"]["cdate"])
        plpy.execute(query)
    else:
        plpy.notice('invalid card')
$$ language plpython3u;

create or replace view cardsview as select * from cards;

create trigger trigger_insert instead of
insert on cardsview
for row execute procedure delete_invalid_cards();

insert into cardsView VALUES(1001, 4509030462, 'Summers PLC', 'credit', 'MIR', 50000, '2000-01-30');
select * from cards where id = 1001;
insert into cardsView VALUES(1002, 4509030462, 'Summers PLC', 'credit', 'MIR', 50000, '1900-01-30');
select * from cards where id = 1002;