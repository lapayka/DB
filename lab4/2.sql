create or replace function getMaxAge()
returns int
as
$$
    query = "select getAge(birthdate) as age from Users"
    result = plpy.execute(query)
    max = -1
    for i in result:
        if (i['age'] > max):
            max = i['age']
    return max
$$ language PLPYTHON3U;


select getMaxAge();