create or replace function sumOver99k()
returns table (id bigint, name varchar, age int)
as
$$
	query = "SELECT Users.passport, Banks.name, Transactions.tsum FROM Banks inner join Transactions on Banks.name = Transactions.bName inner join Users on Users.passport = Transactions.passport WHERE  Transactions.tsum > 99000;"
	result = plpy.execute(query)
	return result
$$ language plpython3u;

select *
from sumOver99k();