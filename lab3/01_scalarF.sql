CREATE OR REPLACE FUNCTION avg_sum() Returns NUMERIC as $average$
DECLARE
	aver numeric;
BEGIN
	select avg(amount) into aver from Cards;
	return aver;
end; 
$average$ LANGUAGE plpgsql;


SELECT avg(amount) as aver from Cards;
select avg_sum();