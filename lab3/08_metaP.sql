CREATE OR REPLACE PROCEDURE get_triggers_metadata()
AS $$
BEGIN
    SELECT trigger_name, event_manipulation, event_object_table
    FROM information_schema.triggers;
END;
$$ LANGUAGE PLPGSQL;