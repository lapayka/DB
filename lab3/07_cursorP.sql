CREATE OR REPLACE PROCEDURE users_age(passport int)
AS $$
DECLARE
    name varchar(30);
    birth date;
    listcur CURSOR (pas int)FOR
        SELECT Users.name, Users.birthDate
        FROM Users
		where Users.credHistory = pas;
BEGIN
    OPEN listcur(passport);
    LOOP
        FETCH listcur INTO name, birth;
        RAISE NOTICE 'passport: % age %', name, extract (year from(now() - birth));
        EXIT WHEN NOT FOUND;
    END LOOP;
    CLOSE listcur;
END;
$$ LANGUAGE PLPGSQL;

