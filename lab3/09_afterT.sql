CREATE OR REPLACE FUNCTION  delete_invalid_transactions() RETURNS TRIGGER
AS $$
BEGIN
    DELETE  from Transactions
    WHERE tsum > 99000;
	
	RAISE NOTICE  'deleted';
	
	Return NEW;
END;
$$ LANGUAGE PLPGSQL;


drop trigger trigger_insert_transactions on Transactions;

CREATE TRIGGER trigger_insert_transactions AFTER
INSERT ON Transactions
FOR ROW EXECUTE PROCEDURE delete_invalid_transactions();

insert into transactions VALUES (1001,  4560214005,  'Summers PLC', '2020-01-30', 'buy', 99500);