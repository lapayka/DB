CREATE OR REPLACE FUNCTION  delete_invalid_cards() RETURNS TRIGGER
AS $$
BEGIN
   if NEW.cDate > ALL(select birthDate from Users where Users.passport = NEW.passport)
   THEN
		INSERT into Cards Values (NEW.id, NEW.passport, NEW.bName, NEW.cardType, NEW.pSystem, NEW.amount, NEW.cDate);
	ELSE 
		RAISE NOTICE 'invalid card';
	END IF;
	
	Return NEW;
END;
$$ LANGUAGE PLPGSQL;


drop trigger trigger_insert on cardsView;


CREATE OR REPLACE VIEW cardsView as select * from Cards;

CREATE TRIGGER trigger_insert INSTEAD OF
INSERT ON cardsView
FOR ROW EXECUTE PROCEDURE delete_invalid_cards();

insert into cardsView VALUES(1001, 4509030462, 'Summers PLC', 'credit', 'MIR', 50000, '2000-01-30');
select * from cards where id = 1001;
insert into cardsView VALUES(1002, 4509030462, 'Summers PLC', 'credit', 'MIR', 50000, '1900-01-30');
select * from cards where id = 1002;