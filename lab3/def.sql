CREATE OR REPLACE PROCEDURE p(cId int)
AS $$
DECLARE
	counter int;
	type varchar(20);
	tD date;
	id int;
	sum int;
    listcur CURSOR (cId int)FOR
        select Transactions.id,  Transactions.ttype, Transactions.tsum, Transactions.cDate
		FROM transactions inner join Cards on transactions.passport  = cards.passport
		where Cards.id = cId
		order by Transactions.cDate DESC;
BEGIN
	counter = 0;
    
	OPEN listcur(cId);
    LOOP
		EXIT WHEN NOT FOUND OR counter = 4;
        FETCH listcur INTO id, type, sum, tD;
        RAISE NOTICE 'id: % type: %, sum: %, date: %', id, type, sum, tD;
		counter = counter + 1;
    END LOOP;
END;
$$ LANGUAGE PLPGSQL;

call p(300);