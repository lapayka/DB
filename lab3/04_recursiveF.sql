CREATE OR REPLACE FUNCTION  banks_subordination()
RETURNS TABLE (name varchar(50), sub_level int)
AS $$
BEGIN
    RETURN QUERY
        with RECURSIVE tCTE(name , parentName , sub_level) as (
            select Banks.name, Banks.parentName, 0
            from Banks
            where Banks.parentName = Banks.name
            union all
            select  Banks.name, Banks.parentName, tCTE.sub_level + 1
            from Banks JOIN tCTE on Banks.parentName = tCTE.name and Banks.parentName != Banks.name
        )
        select tCTE.name, tCTE.sub_level from tCTE;
END;
$$ LANGUAGE PLPGSQL;

select banks_subordination();
