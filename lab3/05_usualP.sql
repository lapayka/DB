CREATE OR REPLACE PROCEDURE p()
AS $$
BEGIN
    UPDATE Transactions
    SET tsum = 1000 + tsum
    WHERE ttype = 'buy';
END;
$$ LANGUAGE PLPGSQL;


select tsum from Transactions WHERE ttype = 'buy';
call p();
select tsum from Transactions WHERE ttype = 'buy';
