CREATE OR REPLACE FUNCTION f()
RETURNS TABLE (passport bigint, amount int, tsum int)
AS $$
BEGIN
    CREATE TEMP TABLE tmp(passport bigint, amount int);

    INSERT INTO tmp(passport, amount)
    SELECT  Users.passport, Cards.amount
     FROM Cards inner join Users on Users.passport = Cards.passport;

    RETURN QUERY
        SELECT  tmp.passport, tmp.amount, Transactions.tsum
		from tmp inner join Transactions on tmp.passport = Transactions.passport
		where Transactions.tsum > tmp.amount;
END;
$$ LANGUAGE PLPGSQL;

select f();