 DROP FUNCTION sumover10k() ;
 CREATE OR REPLACE FUNCTION sumOver10k()
RETURNS TABLE (passport bigint,  name varchar(50), summ int)
AS $$
BEGIN
    RETURN QUERY
		SELECT Users.passport, Banks.name, Transactions.tsum
        FROM Banks inner join Transactions on Banks.name = Transactions.bName inner join Users on Users.passport = Transactions.passport
        WHERE  Transactions.tsum > 99000;
END
$$ LANGUAGE PLPGSQL;

SELECT Users.passport, Banks.name, Transactions.tsum
        FROM Banks inner join Transactions on Banks.name = Transactions.bName inner join Users on Users.passport = Transactions.passport
        WHERE  Transactions.tsum > 99000
		;
		
select sumOver10k();