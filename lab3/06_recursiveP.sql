
CREATE OR REPLACE PROCEDURE  root_bank(cur_name varchar(50))
AS $$
    DECLARE pName VARCHAR(50);
BEGIN
    SELECT name INTO pName
    FROM Banks
    WHERE parentName = cur_name;

    IF pName IS null THEN
        RAISE NOTICE '%: is a root', cur_name;
    ELSE
        CALL root_bank(pName);
    END IF;
END;
$$ LANGUAGE PLPGSQL;